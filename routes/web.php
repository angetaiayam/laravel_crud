<?php

use App\Http\Controllers\About as AboutController;
use App\Http\Controllers\Home as HomeController;
use App\Http\Controllers\Todo as TodoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home.index');
Route::get('/todo', [TodoController::class, 'index'])->name('todo.index');
Route::post('/todo', [TodoController::class, 'store'])->name('todo.store');
Route::get('/todo/create', [TodoController::class, 'create'])->name('todo.create');
Route::get('/todo/edit/{todo}', [TodoController::class, 'edit'])->name('todo.edit');
Route::get('/todo/{todo}', [TodoController::class, 'show'])->name('todo.show');
Route::put('/todo/{todo}', [TodoController::class, 'update'])->name('todo.update');
Route::delete('/todo/{todo}', [TodoController::class, 'destroy'])->name('todo.destroy');
Route::get('/about', [AboutController::class, 'index'])->name('about.index');